package com.demo.fragmentoperations

import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity
import kotlinx.android.synthetic.main.activity_main2.*

class MainActivity2 : AppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main2)
        var fragmen1 = BlankFragment1()
        var fragmen2 = BlankFragment2()
        var fragmen3 = BlankFragment3()

        btnAddFragment1.setOnClickListener {

            supportFragmentManager.beginTransaction()
                .add(R.id.container, fragmen1, fragmen1.javaClass.name).commit()
        }

        btnAddFragment2.setOnClickListener {    supportFragmentManager.beginTransaction()
            .add(R.id.container, fragmen2, fragmen2.javaClass.name).commit() }


        btnAddFragment3.setOnClickListener {  supportFragmentManager.beginTransaction()
            .replace(R.id.container, fragmen3, fragmen3.javaClass.name).addToBackStack(null).commit( ) }
    }
}